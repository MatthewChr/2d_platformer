﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    enum EnemyState
    {
        stand,
        patrol,
        chase,
        stun,
        dead,
    }
    [SerializeField] float detectRadius;
    [SerializeField] Transform player;
    [SerializeField] bool move;
    [SerializeField] EnemyState state;
    [SerializeField] Collider2D footCol;
    [SerializeField] bool grounded;
    [SerializeField] Collider2D wallDetect;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerMovement>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.IsTouchingLayers(footCol, LayerMask.GetMask("Ground"));
        switch (state)
        {
            case EnemyState.stand:
                break;
            case EnemyState.patrol:
                Patrol();
                break;
            case EnemyState.chase:
                break;
            case EnemyState.stun:
                break;
            case EnemyState.dead:
                break;
        }
    }
    private void Patrol()
    {
        if (Physics2D.IsTouchingLayers(wallDetect, LayerMask.GetMask("Ground")))
        {
            transform.localScale = new Vector3(0, 0, -transform.localScale.z);
        }
    }
    private void FixedUpdate()
    {
        if ((player.position - transform.position).sqrMagnitude <= detectRadius * detectRadius && (state == EnemyState.stand || state == EnemyState.patrol))
        {

        }
    }
}
