﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorControl : MonoBehaviour
{
    public Camera cam;
    public Texture2D image;
    private void Start()
    {
        Cursor.visible = false;
        //Cursor.SetCursor(image, Vector2.one * 0.5f, CursorMode.Auto);
    }
    void LateUpdate()
    {
        Vector3 positon = Input.mousePosition;
        positon.z = 5;
        transform.position = cam.ScreenToWorldPoint(positon);
    }
}
