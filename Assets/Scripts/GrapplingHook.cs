﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public DistanceJoint2D joint;
    [SerializeField] LineRenderer line;
    [SerializeField] float debug;
    public Transform angleRef;
    public Transform anchor;
    public void Update()
    {
        if (anchor)
        {
            line.SetPositions(new Vector3[] { transform.position, anchor.position });
            if (joint.distance > 1)
                joint.distance = Vector2.Distance(transform.position, anchor.position);
            else
                joint.distance = 1;
            
        }
    }
    private void FixedUpdate()
    {
        if (anchor && angleRef)
        {
            float angle = Mathf.Atan2(anchor.position.y - transform.position.y, anchor.position.x - transform.position.x) * 180 / Mathf.PI;
            angleRef.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            debug = angleRef.up.y;
        }
    }
    public void ConnectHook(Transform anchor, Rigidbody2D rb)
    {
        this.anchor = anchor;
        joint.connectedBody = rb;
        joint.distance = Vector2.Distance(transform.position, anchor.position);
    }
    public void Disconnect()
    {
        Destroy(gameObject);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(anchor.position, anchor.position + angleRef.up);

    }
}
