﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 8;
    public float jumpForce = 15;
    public bool canJump;
    public float airMove = 0.3f;
    public float friction = 0.2f;
    public float highJumpMulti = 2;
    public float lowJumpMulti = 5f;
    public bool grounded;
    public Vector2 velocity;
    private Vector2 inputDir;
    public float cyoteTime = 0.1f;
    public Collider2D footCol;
    [Space]
    public GameObject hookPrefab;
    public GrapplingHook hook;
    public Camera cam;
    public bool grappled;
    public LayerMask grappleLayers;
    public float grappleSpeed = 12;
    Rigidbody2D rb;
    SpriteRenderer sprite;
    Animator anim;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //sprite = GetComponent<SpriteRenderer>();
        //anim = GetComponent<Animator>();
    }

    void Update()
    {
        inputDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        CheckGround();
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (grappled)
            {
                hook.Disconnect();
                hook = null;
                grappled = false;
            }
            else
            {
                Vector3 angle = cam.ScreenToWorldPoint(Input.mousePosition) - transform.position;

                RaycastHit2D hit = Physics2D.Raycast(transform.position, angle, 20, grappleLayers);
                if (hit.collider != null)
                {
                    hook = Instantiate(hookPrefab, hit.point, Quaternion.identity, hit.collider.transform).GetComponent<GrapplingHook>();
                    hook.ConnectHook(transform, rb);
                grappled = true;
                }
            }
        }
        //AnimUpdate();

    }
    private void FixedUpdate()
    {
        Move();
    }
    private void CheckGround()
    {
        bool g = Physics2D.IsTouchingLayers(footCol, LayerMask.GetMask("Ground"));
        if (!g)
        {
            if (grounded)
            {
                StartCoroutine(CyoteTimer());
                grounded = false;
            }
        }
        else
        {
            grounded = g;
        }

    }
    private void Move()
    {
        if (!grappled)
        {
            if (grounded)
            {
                rb.velocity = new Vector2(inputDir.x * speed, rb.velocity.y);
                canJump = true;
            }
            else
            {
                if (Mathf.Abs(inputDir.x) <= Mathf.Epsilon)
                    if (rb.velocity.x > 0)
                    {
                        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x - friction, 0, speed), rb.velocity.y);
                    }
                    else
                    {
                        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x + friction, -speed, 0), rb.velocity.y);
                    }

                rb.velocity += new Vector2(inputDir.x * speed * airMove * Time.deltaTime * 50, 0);
                if (!Input.GetKey(KeyCode.Space))
                    rb.velocity -= new Vector2(0, Time.deltaTime * lowJumpMulti);
                else
                    rb.velocity -= new Vector2(0, Time.deltaTime * highJumpMulti);
            }


            if (Input.GetKeyDown(KeyCode.Space) && canJump)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Space) && hook.joint.distance > 1)
            {
                Vector3 dir = hook.transform.position - transform.position;
                rb.velocity += new Vector2(dir.x, dir.y).normalized * grappleSpeed;
            }
        }

        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -speed, speed), rb.velocity.y);

        velocity = rb.velocity;
    }
    private void AnimUpdate()
    {
        anim.SetBool("Running", Mathf.Abs(rb.velocity.x) > Mathf.Epsilon);
        if (!grounded)
        {
            if (Mathf.Abs(inputDir.x) > Mathf.Epsilon)
                sprite.flipX = inputDir.x < 0;
        }
        else
        {
            if (Mathf.Abs(rb.velocity.x) > Mathf.Epsilon)
                sprite.flipX = rb.velocity.x < 0;
        }
    }
    IEnumerator CyoteTimer()
    {
        yield return new WaitForSeconds(cyoteTime);
        canJump = false;
    }

}


